Introduction to profiling
=========================

This repository presents a few simple use cases for profiling tools, in particular [perf](http://man7.org/linux/man-pages/man1/perf.1.html) and [valgrind](https://valgrind.org) even though the insights should be good for other profilers. These example focus on CPU and system memory profiling, and does not wander into the GPU territory.


Profiling tools
---------------

There are two main kinds of profiling tools, which have their strength and weaknesses depending on the kind of issue to be profiled:

- sampling profilers, which read regularly the state of the system/software and thus have little impact on the overall performance (depending on the sampling rate)
- simulation-based profilers, which run the software through a virtual machine and thus can check each and every instruction, but has a very high impact on the performance of the software.

`perf` is a sampling profiler, and `valgrind` is a simulation-based profiler.

[gprof2dot](https://github.com/jrfonseca/gprof2dot)
[flamegraph](https://github.com/brendangregg/FlameGraph)


Before starting
---------------

You will need to install a few tools before going through the use case:

```bash
#
# Grab the submodules
#
git submodule update --init --recursive

#
# Install perf
#
# On Ubuntu, build perf by hand as the bundled version does not demangle C++ (at all)
# Note that you must enable source repositories in your package manager, or directly in `/etc/apt/sources.list`
mkdir -p ${HOME}/src/linux && cd ${HOME}/src/linux
apt source linux-tools-$(uname -r)
sudo apt build-dep linux-tools-$(uname -r)
cd linux-$(uname -r | sed 's/-.*//')/tools/perf
make -j$(nproc)
 
# Create an alias for the new perf executable
# A good practice would be to add this to your ~/.bashrc, ~/.zshrc or whatever you use
alias mperf=${HOME}/src/linux/linux-$(uname -r | sed 's/-.*//')/tools/perf/perf
 
# You need the kernel to allow you to get access to the samples
sudo sh -c 'echo -1 >/proc/sys/kernel/perf_event_paranoid'
# And to access kernel information
sudo sh -c " echo 0 > /proc/sys/kernel/kptr_restrict"

#
# Install gprof2dot
#
# Also, install gprof2dot to get nice graphs
sudo apt install graphviz python3-pip
sudo pip3 install gprof2dot
```


List of the use cases
---------------------

1. Process benchmarking using `time`
2. Function benchmarking use `std::chrono`
3. Code profiling using `perf`
4. Perf again, with other tools (gprof2dot and flamegraph)
5. Perf performance counters
6. Memory leak - TODO
7. Thread deadlock - TODO
