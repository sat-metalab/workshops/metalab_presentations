Using std::chrono (a.k.a. poor man profiling)
=============================================

A good way to profile specific parts of a code is to use timers to measure their behavior. `std::chrono` is a good tool for this, and although it can not help you pinpoint quickly what is slowing down your code, it is a good way to profile a code optimization or compare multiple implementation of a given function.

Example: multiple maps implementations
--------------------------------------

This example is an excerpt from [Splash](https://gitlab.com/sat-metalab/splash), and compares multiples maps:
- std::map
- std::unordered_map
- DenseMap, a cache-friendly implementation of a map based on std::vector

Compile and test:
```bash
./build.sh
./main
```

On the test system it can be observed that the DenseMap is slower inserting new entries than the other implementations, which is expected as it needs to go through all the entries to check whether a given key already exists. On the other hand, iteration is faster than std::map (by a factor of 4 on the test system), and slightly faster than std::unordered_map (depending on the map size, it can be up to twice as fast on the test system).

Discussion
----------

As any synthetic test, the performances in the "real world" may/will be different than what we measure with this test. There are multiple reasons for that:
- this process does not have anything else to care about, so the it gets all the attention from the CPU (apart from multitasking). This means that the CPU cache is mostly dedicated to this test, which may decrease the effect of a cache-friendly map
- the compiler may optimize our test, for example it can detect that we do nothing with the variables `key` and `value` except putting value into them. This is why they are declared with the `volatile` keyword: it indicates to the compiler that these variable may be read/written from elsewhere without further notice, so it forces the compiler not to optimize them out. But once again, the compiler may optimize things anyway.
- as any C++ code, the performances greatly varies depending on the optimization flags. This can be easily seen by compiling the test with the debug flag and no optimizations:
```bash
./build_debug.sh
./main
```
Now we see that the DenseMap is slower than both std::map and std::unordered_map for iteration! So as a rule of thumb, profile your code with the compilation flags used in production.
