#include "./io_obj.h"

#include <iostream>

int main(int argc, char** argv)
{
    if (argc != 2)
    {
        std::cout << "Please specify an OBJ file to load\n";
        return 1;
    }

    auto object = Obj(std::string(argv[1]));
    return 0;
}
