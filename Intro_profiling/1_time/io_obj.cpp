/*
 * This file is part of Slaps.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Slaps is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Slaps.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "./io_obj.h"

#include <fstream>
#include <iostream>
#include <regex>

std::vector<double> Obj::loadCoordinates(std::string line)
{
    std::vector<double> coords;
    std::regex reg("\\w*+[^ ]++"); // split on spaces
    auto iter_groups = std::sregex_iterator(line.begin(), line.end(), reg);
    auto iter_end = std::sregex_iterator();
    for (std::sregex_iterator iter = iter_groups; iter != iter_end; iter++)
    {
        std::string match = (*iter).str();
        coords.push_back(std::stof(match));
    }
    return coords;
}

std::vector<Obj::FaceVertex> Obj::loadFaces(std::string line)
{
    std::vector<Obj::FaceVertex> a_face;
    std::vector<double> coords;
    std::regex space_reg("\\w*+[^ ]++");
    auto iter_groups = std::sregex_iterator(line.begin(), line.end(), space_reg);
    auto iter_end = std::sregex_iterator();

    // we split the face by vertex (ie we split on spaces)
    for (std::sregex_iterator iter = iter_groups; iter != iter_end; iter++)
    {
        Obj::FaceVertex face_vertex;
        std::string token = (*iter).str();
        // First part of the token is for sure the vertex id
        face_vertex.vertexID = std::stoi(token);

        // Now we need to check for texture coordinates and normals
        std::regex uv_reg("\\d+\\/(\\d+)");
        std::smatch tmp_match;
        if (std::regex_search(token, tmp_match, uv_reg))
        {
            face_vertex.uvID = std::stoi(tmp_match[tmp_match.size() - 1]);
        }
        std::regex normal_reg("\\/\\/\\d+|\\/\\d+\\/(\\d+)");
        if (std::regex_search(token, tmp_match, normal_reg))
        {
            face_vertex.normalID = std::stoi(tmp_match[tmp_match.size() - 1]);
        }
        a_face.push_back(face_vertex);
    }
    return a_face;
}

Obj::Obj(std::string filename)
{
    std::vector<std::vector<double>> xyzs;
    std::vector<std::vector<double>> uvs;
    std::vector<std::vector<double>> normals;
    std::vector<std::vector<Obj::FaceVertex>> faces;

    // Load obj file
    std::ifstream obj_file(filename, std::ios::in);
    if (!obj_file.is_open())
    {
        std::cout << "Obj:: - " << filename << " could not be opened." << std::endl;
    }

    for (std::string line; std::getline(obj_file, line);)
    {
        std::string::size_type pos;
        if ((pos = line.find("v ")) == 0)
        {
            pos += 2;
            line = line.substr(pos);
            xyzs.push_back(loadCoordinates(line));
        }
        else if ((pos = line.find("vt ")) == 0)
        {
            pos += 3;
            line = line.substr(pos);
            uvs.push_back(loadCoordinates(line));
        }
        else if ((pos = line.find("vn ")) == 0)
        {
            pos += 3;
            line = line.substr(pos);
            normals.push_back(loadCoordinates(line));
        }
        else if ((pos = line.find("f ")) == 0)
        {
            pos += 2;
            line = line.substr(pos);
            faces.push_back(loadFaces(line));
        }
    }

    std::cout << "Obj:: - File: " << filename << "  successfully loaded.\n";
    std::cout << "      - " << faces.size() << " faces loaded\n";
}
