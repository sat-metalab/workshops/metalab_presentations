Using perf
==========

Sometimes, you just don't know what is going on with your code, and where all these CPU cycles go. This is when a full fledged profiler is useful. Here comes `perf`, a sampling profiler bundled with the Linux kernel and which takes advantage of the various system counters available. As a sampling profiler, it checks at regular intervals the state of the software, and writes them to a file which can be later analyzed.

Example: return of the DenseMap
-------------------------------

For this example, we will still use the DenseMap from the previous use case. We saw that the insertion is slow (as expected), but we may improve the performances a bit.

Compile and test:
```bash
./build.sh

mperf record -F 999 -g ./main -o perf.data
mperf report -n -i perf.data
```

You should see an output similar to this. You can navigate in the tree with the arrow keys, and open/close the nodes with the enter key.

```
Samples: 2K of event 'cycles:ppp', Event count (approx.): 10461364445                                                     
  Children      Self       Samples  Command  Shared Object      Symbol                                                    
+   99.90%     0.00%             0  main     [unknown]          [.] 0x2d4e258d4c544155                                    
+   99.90%     0.00%             0  main     libc-2.27.so       [.] __libc_start_main                                     
-   99.87%     0.17%             5  main     main               [.] main                                                  
   - 99.70% main                                                                                                          
      - 98.98% DenseMap<int, float>::insert                                                                               
         - 65.14% DenseSet<int>::find                                                                                     
            + 64.73% std::find<__gnu_cxx::__normal_iterator<int*, std::vector<int, std::allocator<int> > >, int>          
         - 30.39% DenseSet<int>::insert                                                                                   
            + 29.98% std::find<__gnu_cxx::__normal_iterator<int*, std::vector<int, std::allocator<int> > >, int>          
         + 1.23% std::make_pair<DenseMap<int, float>::iterator, bool>                                                     
           0.58% DenseMap<int, float>::iterator::iterator                                                                 
+   98.98%     0.17%             5  main     main               [.] DenseMap<int, float>::insert                          
+   94.71%     0.07%             2  main     main               [.] std::find<__gnu_cxx::__normal_iterator<int*, std::vect
+   94.50%     0.07%             2  main     main               [.] std::__find_if<__gnu_cxx::__normal_iterator<int*, std:
+   85.56%    23.70%           689  main     main               [.] std::__find_if<__gnu_cxx::__normal_iterator<int*, std:
+   65.14%     0.00%             0  main     main               [.] DenseSet<int>::find                                   
...
```

From these results, we can see that `DenseMap::insert` calls `DenseSet::find` twice. And indeed, the method looks like this:

```cpp
std::pair<iterator, bool> insert(std::pair<Key, T>&& value)
{
    auto it = _keys.find(value.first);
    if (it == _keys.end())
    {
        auto keyIt = _keys.insert(value.first);
        _values.emplace_back(value.second);
        return std::make_pair(iterator(_keys.size() - 1, *this), true);
    }

    return std::make_pair(iterator(std::distance(_keys.begin(), it), *this), false);
}
```

`DenseSet::insert` calls `DenseSet::find` too, and it returns a `std::pair<iterator, bool>` which indicates whether the key could be inserted into the set. We can get rid of the first call to `DenseSet::find` as follows:

```cpp
std::pair<iterator, bool> insert(std::pair<Key, T>&& value)
{
    auto result = _keys.insert(value.first);
    if (result.second)
    {
        _values.emplace_back(value.second);
        return std::make_pair(iterator(_keys.size() - 1, *this), true);
    }

    return std::make_pair(iterator(std::distance(_keys.begin(), result.first), *this), false);
}
```

Let's test our modifications:

```bash
# Test optimized version
mperf record -F 999 -g ./main_optimized
mperf report -n -i perf.data
```

And from the results, we can see that `DenseSet::find` is now called only once:

```
Samples: 1K of event 'cycles:ppp', Event count (approx.): 7089245162                                                     
  Children      Self       Samples  Command         Shared Object      Symbol                                            
+   99.92%     0.00%             0  main_optimized  [unknown]          [.] 0x2e1e258d4c544155                            
+   99.92%     0.00%             0  main_optimized  libc-2.27.so       [.] __libc_start_main                             
-   99.77%     0.10%             2  main_optimized  main_optimized     [.] main                                          
   - 99.67% main                                                                                                         
      - 98.36% DenseMap<int, float>::insert                                                                              
         + 95.21% DenseSet<int>::insert                                                                                  
         + 1.26% std::make_pair<DenseMap<int, float>::iterator, bool>                                                    
         + 0.52% DenseMap<int, float>::iterator::~iterator                                                               
           0.51% DenseMap<int, float>::iterator::iterator                                                                
      + 0.60% std::pair<DenseMap<int, float>::iterator, bool>::~pair                                                     
+   98.36%     0.25%             5  main_optimized  main_optimized     [.] DenseMap<int, float>::insert                  
+   95.21%     0.22%             4  main_optimized  main_optimized     [.] DenseSet<int>::insert                         
+   94.08%     0.05%             1  main_optimized  main_optimized     [.] std::find<__gnu_cxx::__normal_iterator<int*, s
+   94.03%     0.05%             1  main_optimized  main_optimized     [.] std::__find_if<__gnu_cxx::__normal_iterator<in
+   84.44%    26.24%           517  main_optimized  main_optimized     [.] std::__find_if<__gnu_cxx::__normal_iterator<in
...
```

On the test system, the results for both non-optimized and optimized versions are as follows:

```
# non-optimized
DenseMap::insert -> 2915117µs
DenseMap::iterator -> 12253µs

# optimized
DenseMap::insert -> 1961666µs
DenseMap::iterator -> 12600µs
```

We get a 33% performance increase, with a minimal change to our code. That's always good to take!


Discussion
----------

First of all, for the function names to be demangled (see [name mangling](https://en.wikipedia.org/wiki/Name_mangling) correctly you need to compile your code with the debug flag and no optimizations. It should be possible to get correct name demangling with some optimizations turned on but it did not work on the test system. For your information, if you want to test it, it may work by doing as follows:

```bash
# -fno-omit-frame-pointer is mandatory here
g++ -std=c++17 -O1 -fno-omit-frame-pointer -I../src main.cpp -o main
perf record --call-graph dwarf -F 999 ./main
perf report -n -i perf.data
```

The same comments regarding optimization and overall performances still apply. So even though you have to compile with debug flags to profile, make sure to test again with the production flags to make sure your modifications did not in the end have a negative impact. You can check whether it is the case by building our example with optimizations:

```bash
./build_O2.sh
./main
./main_optimized
```
