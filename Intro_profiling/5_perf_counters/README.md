Using perf and its performances counters
========================================

The ways of CPUs can be beyond our own reach, but we can still try to understand them better. For example CPUs tend to like data locality due to the multiple cache levels, and this is not an obvious parameter when writing code (whichever the language). Luckily there are tools to help measuring these kind of parameters, and this is how we introduce: performance counters!

`perf` allows for reading a lot of those performance counters, and in this case we will focus on [cache misses](https://en.wikipedia.org/wiki/CPU_cache#Cache_miss) which are one of the most important performance bottleneck.


Example: std::vector >>> all
----------------------------

For this example, we will show how the data structure choice can have a high impact on the overall performances of a code.

Compile and test the original code:
```bash
./build.sh
mperf stat -e cache-references,cache-misses,cycles,instructions,branches,faults,migrations ./main
```

You should get something like this:
```
Looping through std::list<float> -> 3465µs

Performance counter stats for './main':

        1,039,896      cache-references
          661,675      cache-misses              #   63.629 % of all cache refs
      187,534,831      cycles
      400,826,934      instructions              #    2.14  insn per cycle
       81,433,950      branches
            8,325      faults
                0      migrations

      0.061526903 seconds time elapsed
```

If you look at the cache-misses, you see that over 63% of the memory accesses result in the CPU having to go get data from the main memory. In the end, the CPU cache is not correctly used and a lot of CPU cycles are lost waiting for the main memory.

The optimized code replaces the `std::list` with a `std::vector`. Let's test it:
```bash
mperf stat -e cache-references,cache-misses,cycles,instructions,branches,faults,migrations ./main_optimized
```

You should get something like this:
```bash
Looping through std::vector<float> -> 600µs

Performance counter stats for './main_optimized':

          224,814      cache-references
           76,739      cache-misses              #   34.134 % of all cache refs
       21,131,193      cycles
       29,336,443      instructions              #    1.39  insn per cycle
        4,865,513      branches
            2,185      faults
                0      migrations

      0.009573344 seconds time elapsed
```

You see two things: first, it is really faster than the original code just by changing the data structure type. Second, there are a lot less cache misses! For this use-case, it is clear that `std::vector` is a better choice than `std::list`.


Discussion
----------

When the CPU tries to get data from memory, it first checks whether the data is in cache. If not it will go get it in the main memory to put it in cache and use it. The trick is that when it gets the data, it does not get the single data asked at the time but a whole cache line (which size varies with the architecture of the CPU).

What this means is that if we ask for the next data, if it was in the same cache line it will be read directly from the cache, which is vastly faster that the main memory. In our case, `std::list` is a chained list which gives absolutely no guaranty whether the data are tightly packed or not. On the other hand, a `std::vector` gives this guarantee. This leads a better use of CPU cache, and better performances.


More precise measurements
-------------------------

You can get more precise, per-function measurement by using the `record` mode of `perf`:
```bash
mperf record -g -e cache-references,cache-misses,cycles,instructions,branches,faults,migrations ./main
mperf report -n -i perf.data
```
