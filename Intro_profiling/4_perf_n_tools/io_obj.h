/*
 * This file is part of Slaps.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Slaps is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Slaps.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __SLAPS_OBJ__
#define __SLAPS_OBJ__

#include <string>
#include <vector>

class Obj
{
  public:
    struct FaceVertex
    {
        int vertexID{-1};
        int normalID{-1};
        int uvID{-1};
    };

  public:
    Obj() = default;
    Obj(std::string filename);
    bool writeMesh(std::string filename);

  private:
    std::vector<double> loadCoordinates(std::string line);
    std::vector<FaceVertex> loadFaces(std::string line);
};

#endif // __SLAPS_OBJ__
