shmdata example
===============

This is a small example of what shmdata can do and how it works, using the pyshmdata Python module.

There are two scripts:

* `writer.py` which asks for user inputs, and sends the strings through the socket located at `/tmp/pyshmdata_example`.
* `reader.py` which reads data coming through the same socket and prints them.

By default the `datatype` is `text,interesting=yes`. It can be changed with the `--type` option of the writer. By default if there is no `interesting` value in the datatype, or if it is not equal to `yes`, the reader will not show the strings. The expected value can also be changed with the `--interesting` option of the reader.

To quit the reader and the writers all at once, just type "quit" in the reader.
