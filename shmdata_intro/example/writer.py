#!/usr/bin/env python3

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation; either version 2.1
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.

import argparse
import pyshmdata

parser = argparse.ArgumentParser()
parser.add_argument("--datatype")
args = parser.parse_args()

datatype = args.datatype or "text,interesting=yes"

writer = pyshmdata.Writer(path="/tmp/pyshmdata_example", datatype=datatype)

message_count: int = 0

while True:
    data = input("Enter text to send to readers, or \"quit\" to exit: ")
    writer.push(buffer=bytearray(data, encoding="utf-8"))
    if data == "quit":
        break
