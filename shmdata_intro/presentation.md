---
title:
- Introduction à Shmdata
author:
- Nicolas Bouillot et  Emmanuel Durand
institute:
- Société des Arts Technologiques [SAT]
theme:
- default
date:
- 18 juin 2021
aspectratio:
- 169
navigation:
- empty
section-titles:
- false
output:
  revealjs::revealjs_presentation:
    css: styles.css
---

Concepts-clefs
==============

* Interopérabilité
* Flux de donnée 1-vers-n par IPC
* Typage des données

Introduction
============

# Structure de la présentation

* Histoire de Shmdata
* Anatomie d'un shmdata
* Programmer avec Shmdata


1 - Histoire de Shmdata
====

Avant shmdata
====
Au départ il y avait la téléprésence en 3D

![](img/posture1.png)

Problème
====

Les flux vidéo doivent être :

* capturés
* transmis
* affichés
* surveillés
* réencodés
* enregistrés 

Solution
====

Interopérabilité :

* utiliser des outils séparés, mais dédiés aux taches complexes
* assurer la transmission des flux sans latence de transmission en applications

Création de Shmdata :

* librairie multilangage 
* transmission de flux 1-rédacteur-vers-n-lecteurs
* tous types de flux (audio, video, OSC, midi, autres)

Toujours plus de télépresence
====

<iframe src="https://player.vimeo.com/video/304831248" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<p><a href="https://vimeo.com/304831248">Waterfall Music at Linux Audio Conference 2013</a> from <a href="https://vimeo.com/satmetalab">Metalab | SAT</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

Pour qu'il y en ait assez, il y en faut trop
====

<iframe src="https://player.vimeo.com/video/304826675" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<p><a href="https://vimeo.com/304826675">The Drawing Space - Siggraph 2014</a> from <a href="https://vimeo.com/satmetalab">Metalab | SAT</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

L'outil Scenic
====

Téléprésence configurable en direct, par branchement de Shmdata
![](img/scenic.png)

Autres Outils avec Shmdata
====
Splash, ndi2shmdata, addon Blender pour Splash, EiS


2 - Anatomie d'un Shmdata
=====

Tutoriel
====

Laisser rouler dans un terminal :
```
# Produire signal de test video dans un shmdata
gst-launch --gst-plugin-path=/usr/lib/gstreamer-1.0/ videotestsrc ! shmdatasink socket-path=/tmp/video_shmdata
```

Tutoriel
====

Surveiller que des images passent dans le shmdata
```
# monitor a shmdata type and frame sizes
$ sdflow /tmp/video_shmdata
connected: type video/x-raw, format=(string)I420, width=(int)320, height=(int)240, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
0    size: 115200    data: EBEBEBEBEBEBEBEBEBEBEBEBEBEBEB...
1    size: 115200    data: EBEBEBEBEBEBEBEBEBEBEBEBEBEBEB...
etc
```

Tutoriel
====

Voir la vidéo (lancer la même commande dans plusieurs terminaux) :
```
# read the video shmdata and display its content into a window
gst-launch --gst-plugin-path=/usr/lib/gstreamer-1.0/ shmdatasrc socket-path=/tmp/video_shmdata ! xvimagesink
```

Les types des Shmdata
====

Le rédacteur doit spécifier un type pour le Shmdata, par exemple :
```
video/x-raw, format=(string)I420, width=(int)320, height=(int)240,
"framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, 
interlace-mode=(string)progressive, label=(string)\"\\(unexpected\\) \\= chars\\,\""
```

Les types des Shmdata
====

Lors de la connection au rédacteur, le lecteur doit s'assurer que le type mentionné par le rédacteur est celui attendu.

Comment fonctionne une transmission par Shmadata ?
====
Shmdata implémente une logique de transmission à partir des IPCs UNIX :

* Unix Socket (SOC) : envoier de messages en entre programmes (process)
* Shared Memory (MEM): accéder a une même mémoire par plusieurs programmes
* Semaphore (SEM): synchroniser des programmes, pour Shmdata c'est éviter de lire pendant une écriture


Logique de fonctionnement (1/2)
====

Rédacteur :

* (MEM) Allouer la zone de mémoire partagé
* (SOC) Écouter les connections des lecteurs et leur communiquer les informations pour lire (MEM) et se synchroniser (SEM)
* Sur demande de rédaction, se synchroniser avec les lecteurs (SEM), écrire (MEM) et notifier les clients d'une écriture (SOC), 

Logique de fonctionnement (2/2)
====

Lecteur :

* (SOC) Se connecter au rédacteur et obtenir les informations sur MEM et SEM
* (SOC) Quant une écriture est notifiée par le rédacteur, attendre de pouvoir lire (SEM) et lire (MEM) 

Pour aller plus loin (Semaphore)
====

<iframe width="560" height="315" src="https://www.youtube.com/embed/TYnNKdf7cZM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Pour aller plus loin (Shared Memory)
====

<iframe width="560" height="315" src="https://www.youtube.com/embed/qyFwGyTYe-M" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


Plus d'informations
====

Voir la documentation de réference C/C++/Python
```
git clone https://gitlab.com/sat-metalab/shmdata.git
cd shmdata/
mkdir build
cd build
cmake ..
cd ..
doxygen
firefox html/index.html
```

3 - Programmer avec Shmdata
====

Démo avec `writer.py` et `reader.py`

Concepts-clefs --- rappel
====

* Interopérabilité
* Flux de donnée 1-vers-n par IPC
* Typage des données

