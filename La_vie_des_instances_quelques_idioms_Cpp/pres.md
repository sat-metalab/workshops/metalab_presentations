%title: La vie des instances: quelques idioms C++
%author: Nicolas Bouillot
%date: 2021-03-26

-> # La vie des instances: quelques idioms C++ <-

--------------------------------------------------
-> # No pointer in this first part <-
==============

You will learn what is an programming idiom
You will learn basic of class design in C++
You will learn the Resource Acquisition is Initialization (RAII) idiom


--------------------------------------------------
-> # Programming idiom <-
==============

A programming idiom is the usual way to code a task in a specific language.

Example of writing a loop
- for (i=0; i<10; i++)
- for ($i = 1; $i <= 10; $i++)
- (1..10).each


--------------------------------------------------
-> # Constructor and destructor <-
==============

// File *01-foo-class.cpp*
#include <iostream>

class Foo {
 public:
  // constructor
  Foo() { std::cout << "Foo()" << std::endl;}

  //Destructor
  ~Foo() { std::cout << "~Foo()" << std::endl;}
};

int main() {
  Foo obj;
}

// prints :
// Foo()
// ~Foo()

--------------------------------------------------
-> # What happened ? <-
==============

Object are destructed where the scope closes

OK as usual ? Not really.

--------------------------------------------------
-> # The "}" magic <-
==============

int main() {
  std::cout << "before new scope" << std::endl;
  {
    Foo obj;
  }
  std::cout << "after end of scope" << std::endl;
}

// prints :
// before new scope
// Foo()
// ~Foo()
// after end of scope

--------------------------------------------------
-> # The "}" magic <-
==============

Instances are deleted when the scope closes, not latter. 
(there is not garbage collector)


--------------------------------------------------
-> # Leaky FileWriter  <-
==============

File *02-resource-management-made-wrong.cpp*
#include 

class FileWriter {
 public:
  // constructor
  FileWriter() {}

  bool open() { return true; }
  bool write(const std::string& val) { return val.empty() ? false: true; }
  bool close() { return true; }

};

int main(){
  FileWriter j;
  j.open();
  if (!j.write("")) exit(1);
  j.close();  // <- this is not called: leak !
}

--------------------------------------------------
-> # Why is it "wrong" ? <-
==============

- The class user should *never* forget to call close after a successful _open_.
- You have 3 boilerplate lines for a single write
- The user is encouraged to test the _close_, what should be done if _close_ does not return true ?
- What happens is _open_ is called twice, leak ?
- What happens if _write_ is called before _open_?

--------------------------------------------------
-> # Better FileWriter  <-
==============

File *03-better-resource-management.cpp*
#include 

class FileWriter {
 public:
  // constructor
  FileWriter() { std::cout << "open" << std::endl; }
  ~FileWriter(){ std::cout << "close" << std::endl; }
  bool write(const std::string& val) { return val.empty() ? false: true; }
};

int main(){
  FileWriter j;
  if (!j.write("")) exit(1);
}

--------------------------------------------------
-> # Remember what was "wrong" ? <-
==============

None of the following applies:
- The class user should *never* forget to call close after a successful _open_.
- You have 3 boilerplate lines for a single write
- The user is encouraged to test the _close_, what should be done if _close_ does not return true ?
- What happens is _open_ is called twice, leak ?
- What happens if _write_ is called before _open_?

--------------------------------------------------
-> # This is Resource Acquisition Is Initialization (RAII) <-
==============

- holding a resource is a class invariant
- resource allocation (or acquisition) is done during object creation
- resource deallocation (release) is done during object destruction

more From Wikipedia: "RAII is associated most prominently with C++ where it originated, but also D, Ada,[citation needed] Vala,[3] and Rust.[4]"


--------------------------------------------------
-> # OKay but what if open fails ? <-
==============

It would be nice to get a *testable* instance. This can be done with the _safe bool idiom_ 

--------------------------------------------------
-> # Safe bool idiom <-
==============
File *safe-bool-idiom.hpp*

class SafeBoolIdiom {
 public:
  virtual ~SafeBoolIdiom() {}
  explicit operator bool() const;

 private:
 // note \_virtual\_ here require this class to be inherited 
 // by a class implementing the _safe\_bool\_idiom\_ method
  virtual bool safe\_bool\_idiom() const = 0; 
};

SafeBoolIdiom::operator bool() const { return safe\_bool\_idiom(); }

--------------------------------------------------
-> # Now with FileWriter <-
==============

#include <iostream>
#include <string>
#include "./safe-bool-idiom.hpp"

class FileWriter : public SafeBoolIdiom{
 public:
  // constructor
  FileWriter(const std::string& name) {
    if (name.empty()) return;
    is\_open\_ = true;
    std::cout << "open" << std::endl;
  }
  ~FileWriter(){ if (is\_open\_) std::cout << "close" << std::endl; }
  bool write(const std::string& val) { return val.empty() ? false: true; }

 private:
  bool is\_open\_{false};
  bool safe\_bool\_idiom(){ return is\_open\_;}
};

int main(){
  FileWriter file\_opened("/tmp/coucou");
  if (!file) return;
  if (!j.write("aha")) return;
  FileWriter file\_opened2("");
  if (!file\_opened2) std::cout << "file\_opened2 not valid" << std::endl;
}

// prints:
// open
// file\_opened2 not valid
// close

--------------------------------------------------
-> # Safe bool idiom conclusion <-
==============

- It makes instances testable as if they were "Boolean"
- It can be extended with log message (see _BoolLog_ class in switcher)
- It can be extended with any object type (see _BoolAny_ class in switcher)

--------------------------------------------------
-> # OKay, do I realy need a class for this ? <-
==============

Sometimes its overkill to create a class in order to *wrap* the resource
- A library require a _free_ after allocation
- A library require a _close_ after opening
- A library requires an _unref_ after an acquisition
- etc

And sometimes you need to use non RAII classes


--------------------------------------------------
-> # On_scope_exit idiom <-
==============

File *05-on-scope-exit.cpp*
#include <iostream>
#include <string>
#include "./scope-exit.hpp"

int open(const std::string& path) {
  if (path.empty()) return 0;
  std::cout << "open " << path << std::endl;
  return 42;
}

bool close(int val) {
  if (42 != val)
    return false;
  std::cout << "close " << val << std::endl;
  return true;
}


int main() {
  auto a = open("a");
  // "close(a)" will be executed at the end of the scope
  On\_scope\_exit { close(a); };
  auto b = open("");
  if (0 == b) {
    std::cout << "b failled to open, returning" << std::endl;
    return 0;
  }
  close(b);
}

// prints:
// open a
// b failled to open, returning
// close 42


--------------------------------------------------
-> # On_scope_exit idiom implementation <-
==============

#include <utility>

namespace scope\_guard {
template <typename Fun>
class ScopeGuard {
 public:
  explicit ScopeGuard(Fun&& fun) : fun\_(std::move(fun)) {}
  ~ScopeGuard() { fun\_(); }

 private:
  Fun fun\_;
};

enum class ScopeGuardOnExit {};
template <typename Fun>
ScopeGuard<Fun> operator+(ScopeGuardOnExit, Fun&& fn) {
  return ScopeGuard<Fun>(std::forward<Fun>(fn));
}
}  // namespace scope\_guard

#define CONCATENATE\_IMPL(s1, s2) s1##s2
#define CONCATENATE(s1, s2) CONCATENATE\_IMPL(s1, s2)

// could replace \_\_LINE\_\_ with \_\_COUNTER\_\_ but not always available
#define On\_scope\_exit \
  auto CONCATENATE(on\_scope\_exit\_var, \_\_LINE\_\_) = ::scope\_guard::ScopeGuardOnExit() + [&]()

#endif


--------------------------------------------------
-> # Second Part: what about pointers ? <-
==============

TL;DR They act like _open_ & _close_ fonctions !
They are not destructed at the end of the scope

File *06-pointer.cpp*
#include <iostream>

class Foo {
 public:
  Foo() { std::cout << "Foo()" << std::endl;}
  ~Foo() { std::cout << "~Foo()" << std::endl;}
};


int main() {
  Foo* obj = new Foo;
  delete obj;
}

// prints :
// Foo()
// ~Foo()

--------------------------------------------------
-> # Don't forget your call to delete ! <-
==============

File *07-basic-memory-leak.cpp*
#include <iostream>

class Foo {
 public:
  Foo() { std::cout << "Foo()" << std::endl;}
  ~Foo() { std::cout << "~Foo()" << std::endl;}
};


int main() {
  Foo* obj = new Foo;
}

// prints :
// Foo()



--------------------------------------------------
-> # Memory leak <-
==============

Memory leak: memory no longer needed or accessible but not released

Checking memory leaks with valgrind:
$ valgrind --tool=memcheck --leak-check=yes --track-origins=yes ./07-basic-memory-leak
...
==3451960== 1 bytes in 1 blocks are definitely lost in loss record 1 of 1
==3451960==    at 0x483BE63: operator new(unsigned long) (in /usr/lib/x86\_64-linux-gnu/valgrind/vgpreload\_memcheck-amd64-linux.so)
...

--------------------------------------------------
-> # So What ? <-
==============

Use smart pointers !

Smart pointers make pointers acting as "value" instance value, i.e. delete will be called 

--------------------------------------------------
-> # Example ? <-
==============

File *08-smart-pointer.cpp*
#include <iostream>
#include <memory>

class Foo {
 public:
  Foo() { std::cout << "Foo()" << std::endl;}
  ~Foo() { std::cout << "~Foo()" << std::endl;}
  void hello() const { std::cout << "hello" << std::endl; }
};


int main() {
  auto obj = std::make\_unique<Foo>();
  // obj can be used as a raw pointer
  obj->hello();
}

// prints :
// Foo()
// hello
// ~Foo()

--------------------------------------------------
-> # Smart pointers <-
==============

_unique\_ptr_ cannot be copied
_shared\_ptr_ can be copied, and are reference counted


--------------------------------------------------
-> # Last thing: scope for class members <-
==============

File *09-class-member-scope.cpp*
#include <iostream>

class Foo{
public :
  Foo(int val) : val\_(val) { std::cout << "Foo()" << std::endl; }
  ~Foo() { std::cout << "~Foo()" << std::endl; }

private:
  int val\_;
};

class Bar{
public :
  Bar() { std::cout << "Bar()" << std::endl; }
  ~Bar() { std::cout << "~Bar()" << std::endl; }

private:
  Foo foo\_{1};
};

int main() {
  Bar bar;
}

// prints:
// Foo()
// Bar()
// ~Bar()
// ~Foo()


--------------------------------------------------
-> # Scope for class members <-
==============

Class members life is synchronized with Instance Live

--------------------------------------------------
-> # Scope for class members: member reinitialization ? <-
==============

File *10-class-member-reinit.cpp*
#include <iostream>
#include <memory>

class Foo{
public :
  Foo(){ std::cout << "Foo()" << std::endl; }
  ~Foo() { std::cout << "~Foo()" << std::endl; }
};

class Bar{
public :
  Bar() : foo\_(std::make\_unique<Foo>()) { std::cout << "Bar()" << std::endl; }
  ~Bar() { std::cout << "~Bar()" << std::endl; }

  void do\_something() {
    foo\_ = std::make\_unique<Foo>();
  }
private:
  std::unique\_ptr<Foo> foo\_;
};

int main() {
  Bar bar;
  bar.do\_something();
}

// prints:
// Foo()
// Bar()
// Foo()
// ~Foo()
// ~Bar()
// ~Foo()

--------------------------------------------------
-> # Scope for class members: member reinitialization ? <-
==============

You can use pointer member without the need to call _new_ and _delete_
Less error prone

As a general rule, you should *avoid* the use of *new* and *delete*


--------------------------------------------------
-> # More Fun <-
==============

visit https://cppquiz.org

