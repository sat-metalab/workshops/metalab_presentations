#include <iostream>
#include <string>
#include "./scope-exit.hpp"

int open(const std::string& path) {
  if (path.empty()) return 0;
  std::cout << "open " << path << std::endl;
  return 42;
}

bool close(int val) {
  if (42 != val)
    return false;
  std::cout << "close " << val << std::endl;
  return true;
}


int main() {
  int a = open("a");
  // "close(a)" will be executed at the end of the scope
  On_scope_exit { close(a); };
  int b = open("");
  if (0 == b) {
    std::cout << "b failled to open, returning" << std::endl;
    return 0;
  }
  close(b);
}

// prints:
// open a
// b failled to open, returning
// close 42
