#include <iostream>

class FileWriter {
 public:
  // constructor
  FileWriter() {}

  bool open() { return true; }
  bool write(const std::string& val) { return val.empty() ? false: true; }
  bool close() { return true; }

};

int main(){
  FileWriter j;
  j.open();
  if (!j.write("")) exit(1);
  j.close();  // <- this is not called: leak !
}
