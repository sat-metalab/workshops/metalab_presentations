#include <iostream>
#include <memory>

class Foo{
public :
  Foo(){ std::cout << "Foo()" << std::endl; }
  ~Foo() { std::cout << "~Foo()" << std::endl; }
};

class Bar{
public :
  Bar() : foo_(std::make_unique<Foo>()) { std::cout << "Bar()" << std::endl; }
  ~Bar() { std::cout << "~Bar()" << std::endl; }

  void do_something() {
    foo_ = std::make_unique<Foo>();
  }
private:
  std::unique_ptr<Foo> foo_;
};

int main() {
  Bar bar;
  bar.do_something();
}

// prints:
// Foo()
// Bar()
// Foo()
// ~Foo()
// ~Bar()
// ~Foo()
