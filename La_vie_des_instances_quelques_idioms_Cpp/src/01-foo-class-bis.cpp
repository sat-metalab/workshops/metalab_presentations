//compile with
// g++ -o 01-foo-class 01-foo-class.cpp

#include <iostream>

class Foo {
 public:
  Foo() { std::cout << "Foo()" << std::endl;}
  ~Foo() { std::cout << "~Foo()" << std::endl;}
};


int main() {
  std::cout << "before new scope" << std::endl;
  {
    Foo obj;
  }
  std::cout << "after end of scope" << std::endl;
}

// prints :
// before new scope
// Foo()
// ~Foo()
// after end of scope
