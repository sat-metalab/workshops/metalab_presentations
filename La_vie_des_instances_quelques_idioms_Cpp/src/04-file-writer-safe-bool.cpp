#include <iostream>
#include <string>
#include "./safe-bool-idiom.hpp"

class FileWriter : public SafeBoolIdiom{
 public:
  // constructor
  FileWriter(const std::string& name) {
    if (name.empty()) {
      sbi_log_ = std::string("Filename is empty");
      return;
    }
    is_open_ = true;
    std::cout << "open" << std::endl;
  }
  ~FileWriter(){ if (is_open_) std::cout << "close" << std::endl; }
  bool write(const std::string& val) { return val.empty() ? false: true; }

private:
  bool is_open_{false};
  bool safe_bool_idiom() const { return is_open_; }
};

int main(){
  FileWriter file_opened("/tmp/coucou");
  if (!file_opened) return 0;
  if (!file_opened.write("aha")) return 0;
  FileWriter file_opened2("");
  if (!file_opened2) std::cout << "file_opened2 not valid" << std::endl;
}

