%title: Getting started with i3
%author: Nicolas Bouillot
%date: 2020-11-20

-> # Getting started with i3, a Tiling Window Manager <-

--------------------------------------------------
-> # What is a Tiling Window Manager ? <-
==============

a window manager with an organization of the screen into mutually non-overlapping frames

--------------------------------------------------
-> # Install <-
==============

sudo apt install i3

and then logout you window manager session
and then select the i3 windows manager
and then logging
then say yes for generating the contiguration file
then choose between alt and win as i3 key (I suggest win since several software use alt)

--------------------------------------------------
-> # Exit i3 <-
==============

(Use the $mod key for i3-related actions. By defaut `$mod = alt`)

$mod + shift + e : exit i3

--------------------------------------------------
-> # Create and move windows <-
==============

Use the $mod key for i3-related actions. By defaut `$mod = alt`

$mod + enter : terminal
$mod + d : open and application through an auto completion menu
$mod + arrows : change active windows
$mod + shift + arrows : move active window

$mod + shift + q : close current window

--------------------------------------------------
-> # Vertical and Horizontal Modes <-
==============

Default mode for each window is "horizontal".

Horizontal mode : other windows goes only right or left of current window

Vertical mode : other windows can also go below and above 

$mod + v : set vertical mode for current window
$mod + h : set horizontal mode for current window

--------------------------------------------------
-> # Workspaces <-
==============

$mod + <num> : move to workspace "num"
$mod + shift + num : move current window to workspace num


--------------------------------------------------
-> # Workspaces <-
==============

$mod + num : move to workspace "num"
$mod + shift + num : move current window to workspace num

--------------------------------------------------
-> # Float mode <-
==============

But sometime some window does not scale well (qjackctl-ruined-my-life)

$mod + shift + space: set 

$mod + num : move to workspace "num"
$mod + shift + num : move current window to workspace num

$mod + r : enter resize mode
esc: quit resize mode


--------------------------------------------------
-> # Configure i3 <-
==============

Edit the following file

```
~/.config/i3/config
```

$mod + shift + r: reload configuration file, but keep your windows arrangement   

--------------------------------------------------
-> #  Set Background <-
==============

sudo apt install feh
cd ~/.config/i3/config
wget https://nicolasbouillot.net/img/background.jpg

Then add the following to your i3 config file:
set \$wallpaper ~/.config/i3/background.jpg
exec_always "feh --bg-scale \$wallpaper"

Then hit $mod + shift + r
and then the wall paper appears !


--------------------------------------------------
-> # More ! <-
==============

Customizing bar with bash scripts that return values
* change keyboard layout
* display your favorite statistic
* ...

Actually everything can be customized...

--------------------------------------------------
-> # Links ! <-
==============

i3 user guide: https://i3wm.org/docs/userguide.html
Manu's configuration: https://github.com/paperManu/dotfiles/tree/master/i3

