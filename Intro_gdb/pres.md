%title: Basic debug of C/C++ with GDB
%author: Nicolas Bouillot
%date: 2020-11-06

-> # GDB: The GNU Project Debugger <-

--------------------------------------------------
-> # What is GDB ? <-
==============

GDB allows you to see what is going on
*inside* another program while it executes



--------------------------------------------------
-> # Hello World <-
==============

File *01-hello-world.cpp*


#include <iostream>

int main() {
  std::cout << "hello word" << std::endl;
}


--------------------------------------------------
-> # Hello World <-
==============

Compile and run with gdb:

g++ 01-hello-world.cpp -o 01-hello-world 
gdb --args 01-hello-world
(gdb) run 


-------------------------------------------------
-> # See variable when running  <-

File *02-break-and-list.cpp*

int add(int a, int b) {
  return a + b;
}

int main() {
  int result = 0;
  while (result \!= 10) {
    result = add(result, 1);
  }
}


-------------------------------------------------
-> # See variable when running  <-

See incrementation of *result* 

g++ -g 02-break-and-list.cpp -o 02-break-and-list
gdb --args 02-break-and-list
(gdb) break 02-break-and-list.cpp:8
(gdb) run
(gdb) print result
$1 = 0
(gdb) continue
(gdb) print result
$2 = 1



-------------------------------------------------

-> # See the Backtrace after a crash  <-

File *03-backtrace.cpp*

class A {};

void crash() {
  A a;
  delete &a;
}

int main() {
  crash();
}

-------------------------------------------------

-> # See the Backtrace after a crash  <-

Get the backtrace:

gdb --args ./03-backtrace
(gdb) r
free(): invalid pointer
(gdb) backtrace
#5  0x0000555555555195 in crash () at 03-backtrace.cpp:5
(gdb) frame 5
(gdb) print a
$1 = {<No data fields>}


-------------------------------------------------

-> # Threads: let's parallelise crash ! <-



File *04-thread.cpp*

#include <thread>
class A {};

void crash() {
  A a;
  delete &a;
}

int main() {
  std::thread t1(crash);
  std::thread t2(crash);
  std::thread t3(crash);
  std::thread t4(crash);
  std::thread t5(crash);
  std::thread t6(crash);
  std::thread t7(crash);
}


-------------------------------------------------

-> # Threads: let's parallelise crash ! <-

g++ -g -pthread 04-thread.cpp -o 04-thread
gdb --args 04-thread
(gdb) run
free(): invalid pointer
(gdb) bt


See backtrace of other threads (here thread num. 3) 

(gdb) info thread
(gdb) thread 3
(gdb) bt


-------------------------------------------------

-> # Deadlocks <-

File *05-deadlock.cpp*

#include <thread>
#include <mutex>

std::mutex mut; 
void do_something() {
  mut.lock();
}

int main() {
  using namespace std::chrono_literals;
  std::thread t(do_something);
  std::this_thread::sleep_for(100ms);
  mut.lock();
}


-------------------------------------------------

-> # Deadlocks <-

Check with GDB

g++ -g -pthread 05-deadlock.cpp -o 05-deadlock
gdb --args 05-deadlock
(gdb) r

Then type ctrl-z to stop (but not quit) the program

(gdb) bt
(gdb) f 4
(gdb) p mut
$1 = {<std::__mutex_base> = ... __owner = 73485 ...
(gdb) info thread
  Id   Target Id                                       Frame 
 -1    Thread 0x7ffff7a3e740 (LWP 73481)  ...
  2    Thread 0x7ffff7a3d700 (LWP 73485)  ...
(gdb) thread 2
(gdb) bt
...
#3  0x00005555555552c5 in do_something () at 05-deadlock.cpp:10
...
(gdb) frame 3
(gdb) list
8	  mut.lock();
9	  while (true)
10	    std::this_thread::sleep_for(2s);
11	}

