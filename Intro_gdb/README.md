This GDB intro give cpp exemple and gdb session information for debuging C/C++ programs. It gives informations about:

* Compile and run a program with GDB
* Display content of variable when the program is running
* See the backtrace after a crash
* Navigate through multiple threads
* Solve deadlocks thanks to discovery of mutex lock owner 

The markdown presentation is [pres.md](pres.md)

Build and see the presentation
------------------------

```
# install mdp, a terminal based prentation tool
sudo apt install mdp
# Display slides
mdp pres.md
```