class A {};

void crash() {
  A a;
  delete &a;
}

int main() {
  crash();
}
