#include <thread>
class A {};

void crash() {
  A a;
  delete &a;
}

int main() {
  std::thread t1(crash);
  std::thread t2(crash);
  std::thread t3(crash);
  std::thread t4(crash);
  std::thread t5(crash);
  std::thread t6(crash);
  std::thread t7(crash);
}
